FROM debian:stable-slim

LABEL org.opencontainers.image.source  = "https://github.com/metastake/hydradx-node"
LABEL org.opencontainers.image.authors = "team@metastake.cc"

ARG PROG_NAME

COPY ${PROG_NAME} /usr/local/bin/hydra-dx
COPY start.sh /usr/local/bin/start.sh

RUN useradd -m -u 1000 -U -s /bin/bash -d /hydra hydra && \
  mkdir -p /hydra/.local/share && \
  mkdir /data && chown -R hydra:hydra /data && \
  ln -s /data /hydra/.local/share/hydra-dx && \
  chmod +x /usr/local/bin/hydra-dx && \
  chmod +x /usr/local/bin/start.sh && \
  cp /usr/bin/cut /usr/local/bin && \
  rm -rf /usr/bin /usr/sbin

USER hydra
EXPOSE 30333 9933 9944 9615
VOLUME [ "/data" ]

ENTRYPOINT [ "start.sh" ]