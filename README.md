# HydraDX-node

We're here at [MetaStake](https://metastake.cc) running Validator-as-a-Service, Stake-as-a-Service by using Kubernetes on Clouds.

This is the automatic docker build pipeline for [HydraDX-node](https://hydradx.io/).

Released tags can be found [here](https://hub.docker.com/r/metastake/hydradx-node).

[![License](https://img.shields.io/github/license/metastake/hydradx-node)](/LICENSE)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/metastake/hydradx-node?branch=main)](https://gitlab.com/metastake/hydradx-node/-/pipelines)
[![Docker Image Size (latest semver)](https://img.shields.io/docker/image-size/metastake/hydradx-node?sort=date)](https://hub.docker.com/r/metastake/hydradx-node)
[![Docker Image Version (latest semver)](https://img.shields.io/docker/v/metastake/hydradx-node?arch=amd64&sort=date)](https://hub.docker.com/r/metastake/hydradx-node)
[![Docker](https://img.shields.io/docker/pulls/metastake/hydradx-node)](https://hub.docker.com/r/metastake/hydradx-node)

# Flags and Options as Environment variables

| Flags / Options          | Environment Variable            |
|--------------------------|---------------------------------|
| --validator              | VALIDATOR_ENABLED               |
| --discover-local         | VALIDATOR_DISCOVER_LOCAL        |
| --unsafe-rpc-external    | VALIDATOR_UNSAFE_RPC_EXTERNAL   |
| --prometheus-external    | VALIDATOR_PROMETHEUS_EXTERNAL   |
| --base-path              | VALIDATOR_BASE_PATH             |
| --chain                  | VALIDATOR_CHAIN                 |
| --name                   | VALIDATOR_NAME                  |
| --in-peers               | VALIDATOR_IN_PEERS              |
| --out-peers              | VALIDATOR_OUT_PEERS             |
| --max-parallel-downloads | VALIDATOR_MAX_PARALLEL_DOWNLOAD |
| --public-addr            | VALIDATOR_PUBLIC_ADDR           |
| --bootnodes              | VALIDATOR_BOOT_NODES            |